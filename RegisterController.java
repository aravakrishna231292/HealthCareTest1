package com.HealthProject.Registration;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.HealthProject.Dao.PersonDetailsDaoImpl;
import com.HealthProject.Dao.PersondetailsDao;
import com.HealthProject.pojos.CurrentHealth;
import com.HealthProject.pojos.Habbits;
import com.HealthProject.pojos.PatientDetails;

public class RegisterController extends HttpServlet  {

	
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		try {
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		PatientDetails patientdetails=new PatientDetails();
		
		double premium=patientdetails.getPremium();
		double calculatedPremum=premium;
		int persentage=10;
		
		String fullname = request.getParameter("fullname");
		String gender = request.getParameter("gender");
		
		
		//for gender increasing the 2% premium
		calculatedPremum+=getPersentage(premium,persentage-8);
		
		patientdetails.setPremium(premium);
		
		
		int  age = Integer.parseInt(request.getParameter("age"));
				
		//for age increasing the premium
		calculatedPremum=	age<18 ?premium :(age<=25&&age>=18)? calculatedPremum+= getPersentage(premium,persentage):
	(age<=30&&age>=25)? calculatedPremum+= getPersentage(premium,persentage):(age<=35&&age>=30)?calculatedPremum+=  getPersentage(premium,persentage):  
		(age<=40&&age>=35)?calculatedPremum+= getPersentage(premium,persentage): (age>=40)	?calculatedPremum+= getPersentage(premium,persentage+20):calculatedPremum;
		
	
		
		
		
		CurrentHealth currenthealth=new CurrentHealth();
		boolean Hypertension	 = Boolean.parseBoolean(request.getParameter("Hypertension"));
		boolean BloodPressure =      Boolean.parseBoolean(request.getParameter("BloodPressure"));
		boolean BloodSugar	 = Boolean.parseBoolean(request.getParameter("BloodSugar"));
		boolean OverWeight = Boolean.parseBoolean(request.getParameter("OverWeight"));
		
		
		//for pre existing conditions increase the 1% of premium
		
premium=  (Hypertension==true &&BloodPressure==true&&BloodSugar==true&&
				OverWeight==true)?premium+= getPersentage(premium,persentage+1):premium;
				
		
		
		currenthealth.setHypertension(Hypertension);
		currenthealth.setHypertension(BloodPressure);
		currenthealth.setHypertension(BloodSugar);
		currenthealth.setHypertension(OverWeight);
		
		Habbits habbits=new Habbits();
		
		
		boolean smoking	 = Boolean.parseBoolean(request.getParameter("Smoking"));
		boolean alcohol =      Boolean.parseBoolean(request.getParameter("Alcohol"));
		boolean DailyExercise	 = Boolean.parseBoolean(request.getParameter("DailyExercise"));
		boolean drugs = Boolean.parseBoolean(request.getParameter("Drugs"));
		
		
		habbits.setSmoking(smoking);
		habbits.setAlcohol(alcohol);
		habbits.setDailyExercise(DailyExercise);
		habbits.setDrugs(drugs);
		
		//for bad habbits reduce 3% of premium
		calculatedPremum=  (smoking==true &&alcohol==true&&drugs==true&&
				OverWeight==true)?calculatedPremum+= getPersentage(premium,persentage+3):calculatedPremum;
		
				//for good habbits increase 3% of premium
				
				calculatedPremum=  (drugs==true) ?calculatedPremum+=getPersentage(premium,persentage-3):calculatedPremum;	
				
				
				
				
				
				
		
		patientdetails.setPremium(calculatedPremum);
		patientdetails.setAge(age);
		patientdetails.setName(fullname);
		patientdetails.setHabbits(habbits);
		patientdetails.setCurrentHealth(currenthealth);
		
		PersondetailsDao persondao=new PersonDetailsDaoImpl();
		
		
		
		persondao.savePersondetails(patientdetails);
		
		
		
		
		request.setAttribute("personPremium", patientdetails);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/personPremium.jsp");
        dispatcher.forward(request, response); 
		
		//super.service(arg0, arg1);
        
        
		}catch(Exception e) {
			
			//request.setAttribute("personPremium", patientdetails);
	        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/error.jsp");
	        dispatcher.forward(request, response); 
		}
	}
	
public static double getPersentage(double premium,int persentage) {
	
	
	double percentage = premium* persentage/ 100;
	
	
		return percentage;
	}
	
	
	
	
}
