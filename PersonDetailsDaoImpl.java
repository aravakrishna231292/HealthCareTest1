package com.HealthProject.Dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.HealthProject.Util.HibernateUtil;
import com.HealthProject.pojos.PatientDetails;

public class PersonDetailsDaoImpl implements PersondetailsDao{

	
	public PersondetailsDao savePersondetails(Object obj) {
		
		
		SessionFactory sessFact = HibernateUtil.getSessionFactory();
		Session session = sessFact.getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		
		PatientDetails patientdetails=(PatientDetails)obj;
		
		session.save(patientdetails);
		tx.commit();
		
		session.close();
		System.out.println("Successfully inserted");
		sessFact.close();
		
		
		
		return null;
	}

}
