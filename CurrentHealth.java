package com.HealthProject.pojos;

public class CurrentHealth {

	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overWeight;
	
	
	
	
	
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverWeight() {
		return overWeight;
	}
	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}
	
	
	
	
	
	
	
}
