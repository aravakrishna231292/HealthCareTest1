package com.HealthProject.pojos;

public class PatientDetails {

   private	int personid;
	private  double premium=5000;
	private String name;
	private String gender;
	private int age;
	private CurrentHealth currentHealth;
	private Habbits habbits;
	
	
	
	
	
	public int getPersonid() {
		return personid;
	}
	public void setPersonid(int personid) {
		this.personid = personid;
	}
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habbits getHabbits() {
		return habbits;
	}
	public void setHabbits(Habbits habbits) {
		this.habbits = habbits;
	}
	
	
	
}
