package com.HealthProject.Entitys;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name = "Habbits")
public class Habbits implements Serializable {

	
	 private	int personid; 
	private boolean smoking;
	private boolean alcohol;
	private boolean dailyExercise;
	private boolean drugs;
	
	
	public PatientDetails persondetails;
	
	
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "personid", unique = true, nullable = false)
	public int getPersonid() {
		return personid;
	}
	public void setPersonid(int personid) {
		this.personid = personid;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public PatientDetails getPersondetails() {
		return persondetails;
	}
	public void setPersondetails(PatientDetails persondetails) {
		this.persondetails = persondetails;
	}
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	public boolean isDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
	
	public Habbits() {
		
	}
	public Habbits(boolean smoking, boolean alcohol, boolean dailyExercise, boolean drugs) {
		super();
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.dailyExercise = dailyExercise;
		this.drugs = drugs;
	}
	
	
	
	
	
	
}
