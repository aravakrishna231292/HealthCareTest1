package com.HealthProject.Entitys;

import javax.persistence.Entity;
import javax.persistence.FetchType;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PatientDetails")
public class PatientDetails implements Serializable{

	
	
	
	
   private	int personid;
	private  double premium=5000;
	private String name;
	private String gender;
	private int age;
	
	
	private CurrentHealth currentHealth;
	
	
	private Habbits habbits;
	
	
	
	
	@Id
	@GeneratedValue
	@Column(name = "personid")
	public int getPersonid() {
		return personid;
	}
	public void setPersonid(int personid) {
		this.personid = personid;
	}
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "PatientDetails", cascade = CascadeType.ALL)
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "PatientDetails", cascade = CascadeType.ALL)
	public Habbits getHabbits() {
		return habbits;
	}
	public void setHabbits(Habbits habbits) {
		this.habbits = habbits;
	}
	
	
	public PatientDetails(){
		
	}
	
	public PatientDetails(int personid, double premium, String name, String gender, int age,
			CurrentHealth currentHealth, Habbits habbits) {
		super();
		this.personid = personid;
		this.premium = premium;
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.currentHealth = currentHealth;
		this.habbits = habbits;
	}
	
}
