package com.HealthProject.Entitys;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name = "CurrentHealth")
public class CurrentHealth implements Serializable {

	 private	int personid; 
	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overWeight;
	public PatientDetails persondetails;
	
	
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "personid", unique = true, nullable = false)
	public int getPersonid() {
		return personid;
	}
	public void setPersonid(int personid) {
		this.personid = personid;
	}
	
	
	
	@OneToOne(fetch=FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public PatientDetails getPersondetails() {
		return persondetails;
	}
	public void setPersondetails(PatientDetails persondetails) {
		this.persondetails = persondetails;
	}
	
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverWeight() {
		return overWeight;
	}
	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}
	
	public CurrentHealth() {
		
	}
	
	
	
	public CurrentHealth(boolean hypertension, boolean bloodPressure, boolean bloodSugar, boolean overWeight) {
		super();
		this.hypertension = hypertension;
		this.bloodPressure = bloodPressure;
		this.bloodSugar = bloodSugar;
		this.overWeight = overWeight;
	}
	
	
	
	
	
	
	
}
